# logistics.py
# Toby Harris 03/2022

# Brute force the NorthWest Corner heuristic
# rewritten to generalise for scale
# assumes that supply = demand

from itertools import product

# some problems:
problem1 = [         # MWE
[  6,   7,   8,  2],     # each line is a supplier: cost to customer 0, 1, ... 2, total available from this supplier
[ 12,  13,  14,  6],
[  1,   3,  4]           # last line is demand at customer 0, 1, ... 2
]

problem2 = [
[  6,   7,   8,  2], 
[ 12,  13,  14,  6],
[ 17,  16,  15,  3],
[  3,   3,  5]
]

problem3 = [        # as seen in 3P4 EP3 Q5 - the cut down bit we have to work out by hand. Original code solved this in <1s, this takes about 2.
[236, 153, 118, 200],
[156, 110, 167, 400],
[100, 250, 250]
]

problem4 = [        # as seen in 3P4 EP3 Q5. This script, running on a single core, not solved in a hour.
[368, 164, 236, 153, 118, 200],
[228,  90, 156, 110, 167, 400],
[210,  35, 184, 184, 235, 300],
[106, 153, 256, 274, 359, 100],
[150, 300, 100, 250, 250]
]

problem = problem2

n_suppliers = len(problem)-1
suppliernums = range(n_suppliers)
availability = [sup[-1] for sup in problem[:-1]]
demand = problem[-1]
n_customers = len(demand)
customernums = range(n_customers)

print()
print(n_suppliers, "suppliers supplying", n_customers, "customers")
print()
print("availability:", availability)
print()
print("demand:")
print(demand)
print()


def printlines(array):
    for line in array:
        print(line)
    print()


# Extract maximum value for each cell in solution
maxqtys = [[0 for _ in customernums] for _ in suppliernums]

for suppliernum in suppliernums:
    for customernum in customernums:
        maxqtys[suppliernum][customernum] = min(problem[suppliernum][-1], problem[-1][customernum])

print("max quantities:")
printlines(maxqtys)



# Generate all possible distribution profiles for most suppliers within the above found bounds:
# Don't bother with the last supplier as it's profile can be derived from demand

possible_supplier_profiles = [[] for i in range(n_suppliers-1)]

a=0
b=0
for suppliernum in range(n_suppliers-1):
    for supplier_profile in product(*[range(i+1) for i in maxqtys[suppliernum]]):
        a+=1
        if sum(supplier_profile) == problem[suppliernum][-1]:
            possible_supplier_profiles[suppliernum].append(supplier_profile)
            b+=1
            #print(supplier_profile)
    #print()
print("supplier profiles:", a, ", valid supplier profiles:", b)
print()
#printlines(possible_supplier_profiles)



# Generate combinations of these
valid_solutions = []

c=0
d=0
for solution in product(*possible_supplier_profiles):
    solution = list(list(p) for p in solution)   # I just like lists ok
    customers_inbound = [sum(x) for x in zip(*solution)]
    last_supplier_profile = [demand[i] - customers_inbound[i] for i in customernums]
    if (min(last_supplier_profile) >= 0 ) & (max(last_supplier_profile) <= availability[-1]): # and each is within maxqtys...
        d+=1
        solution.append(last_supplier_profile)
        #printlines(solution)
        #print(solution)
        #print()
        valid_solutions.append(solution)
    c+=1

print("profile combinations:", c, ", valid solutions:", d)
print()


# Evaluate the performance of each valid solution 
print("Evaluating performance of valid solutions...")
for soln in valid_solutions:

    cost = sum(sum(problem[suppliernum][i]*soln[suppliernum][i] for i in customernums) for suppliernum in suppliernums)
    soln.append(cost)


print("sorting valid solutions by performance...")
print()
valid_solutions.sort(key=lambda s:s[-1])        # Sort solutions by cost

print("lowest cost:")
printlines(valid_solutions[0])

print("highest cost:")
printlines(valid_solutions[-1])