// Device Programming Activity 2 Toby Harris th598 09/05/2021
// https://cued-partib-device-programming.readthedocs.io/en/latest/activity-2.html#

#include "mbed.h"
#include "stdint.h"          //This allow the use of integers of a known width
#define LM75_REG_TEMP (0x00) // Temperature Register
#define LM75_REG_CONF (0x01) // Configuration Register
#define LM75_ADDR     (0x90) // LM75 address. I'm still surprised it's not 0x48

#define LM75_REG_TOS (0x03) // TOS Register
#define LM75_REG_THYST (0x02) // THYST Register


// Communications
I2C i2c(I2C_SDA, I2C_SCL);
Serial pc(SERIAL_TX, SERIAL_RX);

// LEDs
DigitalOut myled(LED1);
DigitalOut blue(LED2);
DigitalOut alarmled(LED3);

// Interrupt from temperature sensor OS pin
InterruptIn lm75_int(D7);

// Tickers to flash alarm and bool to note when activated (polled by main loop)
Ticker alarm_ticker;
float alarm_rate = 0.2;
bool alarmactivated = false;

// Array to store last 60 samples of temperature data at approx 1s spacing
const int N= 60;
float recenttemps[N];

// Setup I2C read-write memory allocation
int16_t i16; // Working var for temperature conversion
char data_write[3];
char data_read[3];

// Hack an array into a FIFO queue. Does std::queue exist? Instructions said struct must be an array, so here you go:
void FIFO_history(float *queue, float newval) { 
    for (int i=0; (i+1)<N; i++) {  
        queue[i] = queue[i+1]; // for large arrays this must be so slow
    }
    queue[N-1] = newval;
}

void sendlogs() {
    pc.printf("Alarm raised! \n");
    for(int i=0; i<N; i++) {
        pc.printf("%d second(s) into the last minute before alarm temperature was %.3f \r\n",i, recenttemps[i]);   
    }   
}

void togglealarmled() {
    alarmled = !alarmled;
}

void startalarm() { // single use only until power cycle  
    // start flashing alarm
    alarm_ticker.attach(togglealarmled, alarm_rate);
    
    // set flag to transmit logs soon
    alarmactivated = true;
}

int main()
{
        /* Configure the Temperature sensor device STLM75:
           - Thermostat mode Compatator // So edges are not missed, and falling temp does not trigger the alarm.
           - Fault tolerance: 0
           - Interrupt mode means that the line will trigger when you exceed TOS and stay triggered until a register is read - see data sheet
        */
        data_write[0] = LM75_REG_CONF;
        data_write[1] = 0x00;
        int status = i2c.write(LM75_ADDR, data_write, 2, 0);
        if (status != 0) { // Error
            while (1) {
                myled = !myled;
                wait(0.2);
            }
        }
        
        float tos=28; // TOS temperature
        float thyst=26; // THYST tempertuare.

        // This section of code sets the TOS register
        data_write[0]=LM75_REG_TOS;
        i16 = (int16_t)(tos*256) & 0xFF80;
        data_write[1]=(i16 >> 8) & 0xff;
        data_write[2]=i16 & 0xff;
        i2c.write(LM75_ADDR, data_write, 3, 0);

        //This section of codes set the THYST register
        data_write[0]=LM75_REG_THYST;
        i16 = (int16_t)(thyst*256) & 0xFF80;
        data_write[1]=(i16 >> 8) & 0xff;
        data_write[2]=i16 & 0xff;
        i2c.write(LM75_ADDR, data_write, 3, 0);

        // This line attaches the interrupt. The interrupt line is active low so we trigger on a falling edge
        lm75_int.fall(startalarm);
        // I would also compare the just read temperature to threshold on the MCU. Why not have some parallel redundancy in an alarm watchdog?
        
        alarmled=false; // Seems to help if the DigitalOut is given an initial value somewhere.
        
        while(1) { // loop until alarm interrupt
            // Read temperature register
            data_write[0] = LM75_REG_TEMP;
            i2c.write(LM75_ADDR, data_write, 1, 1); // no stop
            i2c.read(LM75_ADDR, data_read, 2, 0);
    
            // Calculate temperature value in Celcius
            int16_t i16 = (data_read[0] << 8) | data_read[1];
            float temp = i16 / 256.0; // Read data as twos complement integer so sign is correct
    
            // Display and log result
            pc.printf("Temperature = %.3f \r\n",temp);
            FIFO_history(recenttemps, temp);
                        
            // blink nominal led
            myled = !myled;
            
            // Rapidly poll alarmactivated. If false, wait one second.
            for (int i=0; i<1000; i++){
                if (alarmactivated) {
                    sendlogs();
                    return 0; // more than break, I want stop, exit, cease!
                }
                wait(0.001); // no wait and an external timeout would be faster, but this is simpler/hackier.
            }
        }
}