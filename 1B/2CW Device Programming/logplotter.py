from matplotlib import pyplot as plt
import serial
from datetime import datetime

board = serial.Serial("COM3", 9600)
alarmdetected = False
recenttemps = []                                # List to receive log
N = 60                                          # Number of datapoints to expect in log

while True:
    line = board.readline()
    #print(line)                                # Includes b' and \r\n. See below link for advice on removal. 
    sline = line.strip()                        # removes trailing \r and \n
    words = sline.decode('ascii').split(' ')    # https://www.reddit.com/r/Python/comments/44qkk5/how_to_convert_bytes_to_string_in_python/
    print(datetime.now().strftime("%H:%M:%S"), words)   # print timestamp and elements of message

    # Save logs
    if alarmdetected == True:
        recenttemps.append(words[-1])           # Temperature value is the last item in the message, when split by whitespaces
        print(len(recenttemps), "log points stored")

    # Detect alarm
    if words[0] == 'Alarm' and words[1] == 'raised!':
        print("Alarm sequence detected!")
        alarmdetected = True

    # stop reading serial after N datapoints
    if len(recenttemps) >= N:
        break
         
plt.plot(recenttemps)
plt.xlabel("Seconds into minute before alarm activation")
plt.ylabel("Temperature deg C")
plt.show()
