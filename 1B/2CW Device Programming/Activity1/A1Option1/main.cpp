#include "mbed.h"

// Send debug info over serial to terminal
Serial pc(SERIAL_TX, SERIAL_RX);

// Green LED
DigitalOut led1(LED1);
// Blue LED
DigitalOut led2(LED2);
// Red LED
DigitalOut led3(LED3);

// Enable interrupts on user button, used to select currently lit colour.
InterruptIn button(USER_BUTTON);

// Debounce button by locking out for fixed duration after rise
Timeout button_debounce_timeout;
float debounce_time_interval = 0.2;

// Set length of cycle to record and then play
const int N = 8;

// Cycle is stored as an array of integers
int led_cycle[N];

// Set counter for which step of the cycle is currently being selected
int step = 0;

// Var to keep track of which LED is currently available for selection
int currentled;

void select_led(int l)
{       
        // Light only the led identified in argument
        if (l==0) {
                led1 = false;
                led2 = false;
                led3 = false;
        }
        if (l==1) {
                led1 = true;
                led2 = false;
                led3 = false;
        }
        else if (l==2) {
                led1 = false;
                led2 = true;
                led3 = false;
        }
        else if (l==3) {
                led1 = false;
                led2 = false;
                led3 = true;
        }
}

// Must define this one twice, as storecurrent calls it 
// but it also calls storecurrent.
void DebounceComplete(); // dummy definition

void storecurrent() {
    //printf disallowed
    
    // Store currently lit led to cycle. 
    // Double clicking will store twice.
    led_cycle[step] = currentled;
    step = step + 1;
    
    // Debounce - disable edge detection for an interval
    button.rise(NULL);
    button_debounce_timeout.attach(DebounceComplete, debounce_time_interval);
}

void DebounceComplete() {
    button.rise(storecurrent);
}

void playback() {
    select_led(0);
    wait(2);
    pc.printf("Playing back \r\n");
    for (int i=0; i<N; i++) {
        pc.printf("Playing back note %d colour %d \r\n", i, led_cycle[i]);
        select_led(led_cycle[i]);
        wait(0.5);
    }
    select_led(0);
    pc.printf("Playback complete \r\n");
}

int main() {
         // Attach callback function to rising edge interrupt
         button.rise(storecurrent);
         
         // Record
         pc.printf("Recording \r\n");
         while(step < N) {
                select_led(currentled);
                pc.printf("LED %d is ON.\r\n", currentled);
                pc.printf("%d Colours are stored.\r\n", step);
                wait(0.5);
                currentled=(currentled%3)+1; // 1, 2, 3, 1, 2, 3
        }
        // Play on loop       
        while(true) {
            playback();
        }
}
