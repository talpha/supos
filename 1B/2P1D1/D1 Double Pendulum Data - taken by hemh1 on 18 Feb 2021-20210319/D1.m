load D1_18Feb2021 
subplot(211)
plot(a1(1,:),a1(2,:),a2(1,:),a2(2,:),a3(1,:),a3(2,:),s(1,:)-0.05,s(2,:),'linewidth',2)
legend('1. Experiment','2. Experiment','3. Experiment','4. Simulation','Location','southwest')
axis([-1 6 -180 180]),grid
xlabel('time (s)'),ylabel('alpha (degrees)')
subplot(212)
plot(a1(1,:),a1(3,:),a2(1,:),a2(3,:),a3(1,:),a3(3,:),s(1,:)-0.05,s(3,:),'linewidth',2)
axis([-1 6 -180 180]),grid
title('D1 Double Pendulum - release from \alpha = 90^\circ   \beta = 0^\circ')
legend('1. Experiment','2. Experiment','3. Experiment','4. Simulation','Location','southwest')
xlabel('time (s)'),ylabel('beta (degrees)')

figure(2)
subplot(211)
plot(m1(1,:),m1(2,:),m1s(1,:)-0.05,m1s(2,:),'linewidth',2)
legend('5. Experiment','6. Simulation','Location','southwest')
axis([-1 6 -180 180]),grid
xlabel('time (s)'),ylabel('alpha (degrees)')
subplot(212)
plot(m1(1,:),m1(3,:),m1s(1,:)-0.05,m1s(3,:),'linewidth',2)
axis([-1 6 -180 180]),grid
title('D1 Double Pendulum - release from \alpha = 15^\circ   \beta = 30^\circ')
legend('5. Experiment','6. Simulation','Location','southwest')
xlabel('time (s)'),ylabel('beta (degrees)')

figure(3)
subplot(211)
plot(m2(1,:),m2(2,:),m2s(1,:)-0.05,m2s(2,:),'linewidth',2)
legend('7. Experiment','8. Simulation','Location','southwest')
axis([-1 6 -180 180]),grid
xlabel('time (s)'),ylabel('alpha (degrees)')
subplot(212)
plot(m2(1,:),m2(3,:),m2s(1,:)-0.05,m2s(3,:),'linewidth',2)
axis([-1 6 -180 180]),grid
title('D1 Double Pendulum - release from \alpha = 10^\circ   \beta = -50^\circ')
legend('7. Experiment','8. Simulation','Location','southwest')
xlabel('time (s)'),ylabel('beta (degrees)')
