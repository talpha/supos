# th598 01/2021 Materials lab M3 task 1.3
# Run on local machine, python 3x


# standard imports
#none

# installed imports
import numpy as np
from ase.build import bulk
from ase.units import GPa
from matplotlib import pyplot as plt

# local imports
import Morse

# Aux functions
def create_lattice_model():
    latticemodel = bulk("Cu", "fcc", a=3.6028788, cubic=True)               # Init at approx equlibrium spacing. Found to be 2.6163 in the previous exercise, but 3.6 suggested in 1.3 instructions?
    latticemodel.set_calculator(Morse.MorsePotential())                     # 3.6028788 found from zero pressure iteration
    return latticemodel

def set_hydrostatic_strain(model, strain):
    cell = model.get_cell()
    cell *= (1+strain)
    model.set_cell(cell, scale_atoms=True)

def get_hydrostatic_pressure(model):
    press = model.get_stress()[0]                                            # Supplied in Voigt notation. The diagonal repeats the same value and is the only one required. 
    return press/GPa                                                         # Convert to GPa. The "GPa" constant has a value ~= 1/160, and multiplying by 160 converts from eV/A^3 to GPa

def get_hydrostatic_potential(model):
    return model.get_potential_energy()/model.get_global_number_of_atoms()  # is the "global" necessary? What does it do?
    

# Task functions
def plot_lattice_potential_and_pressure():

    # init variables for plot
    linear_strains = []
    volumetric_strains = []
    pressures = []
    potentials = []
    
    # iterate over variables for plot
    for strain in np.linspace(-0.05, 0.05, 100):                            # Specify 1D strain to apply
        linear_strains.append(strain)
        volumetric_strains.append((1+strain)**3 - 1)                        # Be more accurate than neglecting strain products and only tripling strain.
        cu = create_lattice_model()                                         # Use a fresh model for every strain - otherwise the latest strain transformation is stacked with all previous!
        set_hydrostatic_strain(cu, strain)
        pressures.append(-get_hydrostatic_pressure(cu))
        potentials.append(get_hydrostatic_potential(cu))

    # Plot results
    fig, ax1 = plt.subplots()
    color = 'tab:red'
    ax1.set_xlabel("Volumetric Strain")
    ax1.set_ylabel('Potential per atom, eV', color=color)
    ax1.plot(volumetric_strains, potentials, color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:blue'
    ax2.set_ylabel('Hydrostatic Pressure, GPa', color=color)
    ax2.plot(volumetric_strains, pressures, color=color)
    ax2.tick_params(axis='y', labelcolor=color)
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.show()

def calculate_bulk_modulus(model=create_lattice_model(), vstrain = -0.00001):
    # All required information is in the model's current state. Use a fresh crystal at equlibrium by default. 
    # K = -deltaP/vstrain. No need to know the volume per atom etc. Experimental value of 123 GPa for cu. This returns about 104.
    initial_pressure = get_hydrostatic_pressure(model)              # Near zero if equlibrium model used
    set_hydrostatic_strain(model, vstrain/3)                        # Small strain assumption: products of strains are neglible
    strained_pressure = get_hydrostatic_pressure(model)
    return (strained_pressure - initial_pressure)/vstrain           # First two terms are swapped (again!) as denominator negative (if compressing)


# Test & Run
plot_lattice_potential_and_pressure()
print("Bulk modulus is", calculate_bulk_modulus())