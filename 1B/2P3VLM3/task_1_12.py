# th598 01/2021 Materials lab M3 task 1.1 and 1.2
# Run on local machine, python 3x

# standard imports
from math import copysign, isnan

# installed imports
import numpy as np
from ase import Atoms
#from ase.units import eV Ang, GPa                              # These do not seem necessary as Morse and Atoms are already expecting floats in these units
from matplotlib import pyplot as plt

# local imports
import Morse

# utility & wrapper functions
def create_biatomic_model():                                                        # I'm working with ASE in the most messy way. The need to pass the model between functions is not neat. 
    a = Atoms('2Cu', positions=[(0,0,0), (0,0,0)])                                  # init with two atoms in the same place
    calc = Morse.MorsePotential()
    a.set_calculator(calc)
    return a

def get_biatomic_force(separation_distance, atoms=create_biatomic_model()):         # Create a new atomic model if one is not supplied. 
    atoms.positions[1,2] = separation_distance                                      # No further scaling required as expecting data in Ang
    return atoms.get_forces()[0,2]

def get_biatomic_potential(separation_distance, atoms=create_biatomic_model()):
    atoms.positions[1,2] = separation_distance                                      # No further scaling required as expecting data in Ang
    return atoms.get_potential_energy()

def numerical_differentiation(fun, point, perturb=0.000001):                        # If peturb is small (<10-6) the finite float depth in this calculation outweights the resolution of nonlinearity 
    dF = fun(point+perturb/2) - fun(point-perturb/2)
    return dF/perturb

def get_potential_gradient(separation_distance):                                    # Because although I can pass functions as arguments to functions, 
    return numerical_differentiation(get_biatomic_potential, separation_distance)   # I haven't mastered nesting these infinitely recursively. Decorators required?
                                                                                    # Wrapper of the above two funcs. 

def iterate_around_root(fun, trial_bounds, max_iterations):                         # Rather like scipy.optimise.bisect
    lower_iteration, upper_iteration = trial_bounds
    for i in range(max_iterations):                                                 # Why be interesting an use a recursive function when you can be simple?
        mid_iteration = (lower_iteration + upper_iteration)/2                       # Location of midpoint of the test range
        mid_evaluation = fun(mid_iteration)                                         # Evaluation of the function at this midpoint
        if copysign(1, mid_evaluation) == copysign(1, fun(lower_iteration)):        # Tighten bounds accordingly: if trial result is on the same side of the root as the lower bound,
            lower_iteration = mid_iteration                                         # move the lower bound in to the trial location. 
        elif mid_evaluation == 0:                                                   # Not that it will happen with py floats, except when function cannot be evaluated accurately. 
            #print("exact zero reached")
            break
        else:
            upper_iteration = mid_iteration

    return mid_iteration


# Task functions
def plot_force_and_energy():

    # Define model - not that this saves visible computing time compared with recreating for each point!
    a = create_biatomic_model()

    # init variables for plot
    separations = []
    potentials = []
    forces = []

    # iterate over variables for plot
    for separation_distance in np.arange(2.25, 5, 0.01):             # > 0.5 reasonable, but >2.2 shows curve best
        separations.append(separation_distance)
        forces.append(get_biatomic_force(separation_distance, a))
        potentials.append(get_biatomic_potential(separation_distance, a))
        #print("Sep", separation_distance, "Force", forces[-1], "Potential:", potentials[-1])

    # Plot results
    fig, ax1 = plt.subplots()
    color = 'tab:red'
    ax1.set_xlabel("Centre seperation, Ang")
    ax1.set_ylabel('Potential, eV', color=color)
    ax1.plot(separations, potentials, color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:blue'
    ax2.set_ylabel('Force, eV/Ang', color=color)
    ax2.plot(separations, forces, color=color)
    ax2.tick_params(axis='y', labelcolor=color)
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.show()

def find_minimums():
    
    bounds = [2, 4]
    print("Within separation bounds", bounds, ":")

    # Find zero potential separation
    print("     Zero potential found at separation", iterate_around_root(get_biatomic_potential, bounds, 100))

    # Find zero force separation
    print("     Zero force found at separation", iterate_around_root(get_biatomic_force, bounds, 100))

    # Find minimum potential separation
    print("     Minimum potential found at separation", iterate_around_root(get_potential_gradient, bounds, 100))


def compare_force_potential_derivative(separation):
    
    potgrad = get_potential_gradient(separation)
    if potgrad:                                                     # Catching errors derived from division by zero due to irresolvably small potgrad
        return (get_biatomic_force(separation) - potgrad)/potgrad
    else: 
        return float('nan')                                                                  

def plot_force_potential_derivative_error():
    
    # init variables for plot
    separations = []
    discrepancies = []

    # iterate over variables for plot
    for separation_distance in np.arange(0.001, 5, 0.001):
        separations.append(separation_distance)
        discrepancies.append(compare_force_potential_derivative(separation_distance))

    # Plot results
    plt.plot(separations, discrepancies)
    plt.ylabel("Relative error of Atoms.get_forces() compared with Atoms.get_potential_energy()")
    plt.xlabel("Biatomic separation (Ang)")
    plt.show()


# Test & Run
plot_force_and_energy()                            # task 1.1
find_minimums()                                    # task 1.1
#print(compare_force_potential_derivative(2))      # task 1.2
plot_force_potential_derivative_error()            # task 1.2