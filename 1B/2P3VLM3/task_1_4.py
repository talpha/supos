# th598 01/2021 Materials lab M3 task 1.4
# Run on local machine, python 3x


# standard imports
#none

# installed imports
import numpy as np
from ase.units import GPa

# local imports
from task_1_3 import create_lattice_model
from task_1_12 import iterate_around_root

# Aux functions
def trans_stress(poisson_ratio, strain=0.1):
    # returns the transverse stress under axial strain with Poisson ratio specified.

    cu = create_lattice_model()                                     # Create a fresh lattice
    cua = np.array(cu.get_cell())                                   # Translate lattice into array
    cua[0][0] *= (1+strain)                                         # Move an face of the cell to apply primary strain
    cua[1][1] *= (1-strain*poisson_ratio)                           # Move an face of the cell to apply secondary strain
    cua[2][2] *= (1-strain*poisson_ratio)                           # Move an face of the cell to apply secondary strain
    cu.set_cell(cua, scale_atoms=True)                              # Update the model with this motion
    return cu.get_stress(False)[1][1]                               # Return the transverse stress ([2][2] should also be the same value)


# Task functions
def calculate_shear_modulus(model=create_lattice_model(), strain=0.0001):

    modelarray = np.array(model.get_cell())                         # Translate lattice into array
    modelarray[0][1] = modelarray[0][0]*strain                      # Move an face of the cell to apply shear strain
    model.set_cell(modelarray, scale_atoms=True)                    # Update the model with this motion
    return model.get_stress(False)[0][1] / (strain*GPa)             # Scale force to GPa and return shear modulus

def calculate_poisson_ratio(model=create_lattice_model(), strain=0.01):
    return iterate_around_root(trans_stress, (0,0.5), 100)          # Very handy function I made earlier
    

# Test & Run
print("Shear Modulus is", calculate_shear_modulus())
print("Poisson Ratio is", calculate_poisson_ratio())