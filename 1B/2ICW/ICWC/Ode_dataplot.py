import matplotlib as mpl
from matplotlib import pyplot as plt
import numpy as np

stresses = np.array([
0,
24.78,
74.34,
123.90,
247.80,
371.71,
495.61,
371.71,
247.80,
123.90,
74.34,
24.78,
0
])

strain40 = np.array([
0,
0.10,
0.21,
0.31,
0.42,
0.52,
0.62,
0.62,
0.52,
0.42,
0.42,
0.31,
0.31
])

strain50 = np.array([
0,
0.10,
0.31,
0.42,
0.73,
0.83,
1.04,
0.94,
0.83,
0.73,
0.63,
0.52,
0.52
])

strain60 = np.array([
0,
0.10,
0.21,
0.31,
0.52,
0.62,
0.83,
0.73,
0.62,
0.52,
0.52,
0.42,
0.31
])

strain80 = np.array([
0,
0.10,
0.21,
0.31,
0.42,
0.52,
0.62,
0.62,
0.52,
0.42,
0.42,
0.31,
0.21
])

plt.figure()
plt.xlabel("Strain, %")
plt.ylabel('Sand Stress, kPa')

y = stresses
x = strain40
plt.quiver(x[:-1], y[:-1], x[1:] - x[:-1], y[1:] - y[:-1], scale_units='xy', angles='xy', scale=1, width=0.003)
x = strain50
plt.quiver(x[:-1], y[:-1], x[1:] - x[:-1], y[1:] - y[:-1], scale_units='xy', angles='xy', scale=1, width=0.003, color='red')
x = strain60
plt.quiver(x[:-1], y[:-1], x[1:] - x[:-1], y[1:] - y[:-1], scale_units='xy', angles='xy', scale=1, width=0.003, color='blue')
x = strain80
plt.quiver(x[:-1], y[:-1], x[1:] - x[:-1], y[1:] - y[:-1], scale_units='xy', angles='xy', scale=1, width=0.003, color='green')

plt.show()
