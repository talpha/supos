# Processes and Plots data obtained by anaslosher.py

import matplotlib as mpl
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import os

datadirectory = "c:/Users/LocalAdmin/Documents/gitlab/supos/supos/2ICW/ICWEE/Sloshdata0603"

minreadings = []
maxreadings = []
freqs = []

mpl.rcParams.update({'font.size': 15})

for datafile in os.listdir(datadirectory):
    
    # Extract data, Pandas style
    slg = pd.read_csv(datadirectory + "/" + datafile)
    sloshtimes = slg.to_numpy()[:,0]
    sloshin = slg.to_numpy()[:,2]
    sloshout = slg.to_numpy()[:,1]
    outmin = int(min(sloshout))
    minreadings.append(outmin)
    outmax = int(max(sloshout))
    maxreadings.append(outmax)

    # Window datasets
    window = np.blackman(len(sloshout))
    sloshinw = window*sloshin
    sloshoutw = window*sloshout
    
    # Perform FFT
    sloshinfft = np.fft.rfft(sloshinw)
    sloshoutfft = np.fft.rfft(sloshoutw)
    freq = np.fft.rfftfreq(sloshtimes.shape[-1], 0.01)

    # Discard first FFT terms as don't care about static case (they just make a mess of peak finding) and take positive value (is there an imaginary part being discarded?)
    # Windowing also adds leakage around 0Hz so increase HP filter slightly further
    sloshinfft = abs(sloshinfft[3:])
    sloshoutfft = abs(sloshoutfft[3:])
    freq = freq[3:]

    # Faff with axes, titles/labels and gridspec for layout
    fig = plt.gcf()                                             # Almost identical to plt.figure()
    figtitle = slg.columns[3] + " " + slg.columns[4] + " V"     # "Motor voltage" and value read from csv
    fig.suptitle(figtitle)
    fig.canvas.set_window_title("Water tank frequency response: " + figtitle)
    gs = fig.add_gridspec(2,2)
    ax1 = fig.add_subplot(gs[0, 0])
    ax1.set_title(slg.columns[1] + ", range " + str(outmin) + "-" + str(outmax))
    #ax1.set_xlabel("Time (seconds)")
    ax1.set_ylabel("ADC reading")
    ax2 = fig.add_subplot(gs[1, 0])
    ax2.set_title(slg.columns[2])
    ax2.set_xlabel("Time (seconds)")
    ax2.set_ylabel("ADC reading")
    ax3 = fig.add_subplot(gs[:, 1])
    ax3.set_title("Windowed DFTs")
    ax3.set_xlabel("Frequency (Hz)")

    # Plot data and FFTs
    ax1.plot(sloshtimes, sloshout, color='blue')
    ax2.plot(sloshtimes, sloshin, color='orange')
    ax3.plot(freq, sloshinfft, color='orange')
    ax3.plot(freq, sloshoutfft, color='blue')
    
    # Show peaks and response ratio
    infftmax = np.abs(sloshinfft).max()
    outfftmax = np.abs(sloshoutfft).max()
    infftmaxlocation = freq[np.where(sloshinfft == infftmax)[0][0]]
    freqs.append(infftmaxlocation)
    ax3.legend(["Peak at " + str(round(infftmaxlocation, 3)) + " Hz", "Max response " + str(int(round(outfftmax)))])

    # Format and show
    ax3.set_xlim(left=0, right=20)                              # Manaually set
    ax3.set_ylim(bottom=0, top=40000)                          # Manually set
    plt.get_current_fig_manager().window.state('zoomed')        # Start fullscreen
    plt.show()

# Finally, show a plot of how the max height varied with frequency
fig, ax = plt.subplots()
ax.set_xlabel("Frequency (Hz)")
ax.set_ylabel("ADC reading")
ax.set_title("Extreme readings for each frequency")
ax.plot(freqs, maxreadings) # = min height
ax.plot(freqs, minreadings) # = max height
ax.legend(["Minimum Heights", "Maximum Heights"])

# Include range
ranges = [maxreadings[i] - minreadings[i] for i in range(len(maxreadings))]
ax2 = ax.twinx()
color = 'red'
ax2.set_ylabel("Range", color=color)
ax2.tick_params(axis='y', labelcolor=color)
ax2.plot(freqs, ranges, color=color)

plt.get_current_fig_manager().window.state('zoomed')        # Start fullscreen
plt.show()