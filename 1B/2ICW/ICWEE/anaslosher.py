# Raspi Zero datalogging of analogue input

# Standard imports
import time
from time import sleep

# Installed imports
import numpy as np
import RPi.GPIO as io

# Local imports
from alphatools.mcp3008 import MCP3008
from alphatools.Alphalog import CSV_Datalog
from alphatools.toolbox import isclosetoint

# Settings
duration = 10 # s
samplerate = 100
Vref = 3.35 # V                                         # Fed from pi's 3v3 reg. Read with multimeter.

# Pinout
electrode_power_pin = 17                                # BCM pin number for supplying voltage to the electrode and shunt
IRLED_powerpin = 27                                     # BCM pin number for supplying power to the IR LED and phototransistor

electrode_channel = 0                                   # ADC channel for determining resistance of fluid tank
lightgate_channel = 4                                   # ADC channel to measure lightgate state for input phase and frequency
motor_channel = 7                                       # ADC channel to directly measure voltage applied to motor. Don't exceed Vref!

# Setup
io.setmode(io.BCM)
io.setwarnings(False)
io.setup(electrode_power_pin, io.OUT)
io.setup(IRLED_powerpin, io.OUT)

numsamples = duration*samplerate + 1
t = np.linspace(0, duration, numsamples)                # Evenly spaced timesteps at previously specified samplerate and duration

# Run
with MCP3008() as adc:

    print("Reading motor voltage...")
    motor_voltage = round(adc.read(motor_channel, average=10)*Vref/1024, 3)                 # Sample motor voltage and convert to volts
    datalog_name =  str(motor_voltage) + " " + str(int(time.time()-1615070000)) # Include motor voltage and timestamp in datalog name
    
    print("Creating datalog...")
    datalog = CSV_Datalog("/home/pi/Sloshlogs/Sloshlog " + datalog_name)        # Create datalog
    datalog.log("Elapsed Time", "Electrode Voltage Fraction", "Lightgate Voltage Fraction", "Motor Voltage:", motor_voltage)
    datalog.log_dump()                                  # Datalog headers

    print("Starting experiment...")
    io.output(electrode_power_pin, 1)                   # Activate electrode
    io.output(IRLED_powerpin, 1)                        # Activate lightgate
    sleep(3)                                            # Allow bridges to stabilise    

    start_time = time.time()                            # Set zero time
    for elapsed in t:
        
        while time.time() < start_time + elapsed:       # Wait until it is time to take the sample
            pass
        
        electrodevoltage = adc.read(electrode_channel)  # Take all readings
        lightgatevoltage = adc.read(lightgate_channel)

        if isclosetoint(elapsed):                       # Display progress every second
            print("Elapsed time:", round(elapsed))

        datalog.log(elapsed, electrodevoltage, lightgatevoltage)   # Store data

print("Experiment complete")
io.output(electrode_power_pin, 0)                       # Shutdown electrode to avoid excessive electrolysis!
io.output(IRLED_powerpin, 0)                            # Shutdown LED and phototransistor to save power
datalog.log_dump()                                      # Offload data to file
print("Data saved")