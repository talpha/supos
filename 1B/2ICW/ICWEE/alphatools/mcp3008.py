# mcp3008.py
# 10 bit SPI ADC 
# There are plenty of libs for this on the web but I can't get any to compile on my pi!
# So I've resorted to writing my own. It's very simple. 

# Usage notes:
# supply one channel number (0-7) for single ended input or two for software pesudo differential.

# from mcp3008 import MCP3008
# adc = MCP3008
# se_value = adc.read(1)
# pd_value = adc.read(1,0)


# Imports (source unknown?)
from spidev import SpiDev
    
class MCP3008:
    def __init__(self, bus = 0, device = 0, speed_hz = 1000000):            # 1MHz
        self.bus, self.device = bus, device                                 # Store address in class, although unused
        self.spi = SpiDev()                                                 # Default mode is correct
        self.spi.open(bus, device)
        self.spi.max_speed_hz = speed_hz

    def __enter__(self):                                                    # Context manager support eg with block
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
 
    def read(self, positive_channel = 0, negative_channel=None, average=1):
        data = []
        for i in range(average):
            value = self.readraw(positive_channel)
            if negative_channel is not None:                                # Software pseudo differential support
                value -= readraw(negative_channel)
            data.append(value)
        return sum(data)/len(data)                                          # Return the mean average

    def readraw(self, channel):
        adc = self.spi.xfer2([1, (8 + channel) << 4, 0])                    # Patch positive channel
        return ((adc[1] & 3) << 8) + adc[2]                                 # Read a byte and two bits
            
    def close(self):
        self.spi.close()