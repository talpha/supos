# Alphalog 2.2
# Toby Harris 2020-1
# Includes bulk datalogging

import sys
import os
from datetime import datetime
import csv

class AlphaSystemLog:
    """Write statements to a text file with timestamps. Default file name is __main__ + Log.txt."""

    # Usage:
    # from alphatools.Alphalog import AlphaSystemLog
    # syslog = AlphaSystemLog(ForcePath = None, logprinting=True)
    # syslog.boot()
    # while loop:
    #     syslog("my script wrote this to the log")

    def __init__(self, ForcePath = None, logprinting=True):
        self.logprinting = logprinting
        if ForcePath: # sometimes when running as a different user the log files will not find the relative directory that they usually do. Use this to force an absolute reference. 
            self.logname = ForcePath + os.path.basename(sys.argv[0]) + " Log.txt"
        else:
            self.logname = os.path.basename(sys.argv[0]) + " Log.txt"
    
    def __enter__(self):                                                    # Context manager support eg with block
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __call__(self, *logdatas):
        logdata = "".join(str(x) for x in logdatas)         
        logline = str(datetime.now()) + " " + logdata
            
        if self.logprinting:
            print(logline)

        with open(self.logname, "a") as f:
            f.write(logline)
            f.write("\n")

    def boot(self):
        self()
        self("*"*50)
        self(file_name, " booting")
        self()
        

class CSV_Datalog:
    """Wrapper for writing to a CSV file."""
    
    # Usage: 
    # from alphatools.Alphalog import CSV_Datalog
    # datalog = CSV_Datalog("path/datalogname")
    # datalog.log(header1, header2)
    # datalog.log_dump()
    #
    # while loop:
    #     for i in range(n): # high speed section
    #         datalog.log(datatolog, moredatatolog)
    #    
    #     datalog.log_dump()
    #     time.sleep(5)

    def __init__(self, DataLogName):
        self.DataLogName = DataLogName
        self.datalog_RAM = []

        # Test opening file/creating it
        with open(self.DataLogName + '.csv', 'a', newline='') as datalog:
            pass # the datalog has been opened, or an error has been raised.

    def __enter__(self):                                                    # Context manager support eg with block
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.log_dump()                                                     # Save any volatile data before closing class instance
        # No file closeure needed as file is closed after every log_dump operation

    def log(self, *data): # Name is confusing with previous class, but likely to be with class called 'data'
        self.datalog_RAM.append(i for i in data) # Only saves in a py variable until the below log_dump is called to save it.

    def log_dump(self): # When running high speed loops, the time to open the csv is significant. The log_dump system allows controlling this operation frequency.
        with open(self.DataLogName + '.csv', 'a', newline='') as datalog:
            datalogger = csv.writer(datalog, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)        
            for line in self.datalog_RAM:         
                datalogger.writerow([i for i in line])
            self.datalog_RAM.clear()
