# toolbox.py
# some handy maths tools
# Toby Harris 2020

# standard imports
import math
from math import log as ln
import datetime

# functions
def round_sf(num, sig_figs, returnstring=True):
    if num == 0:
        if returnstring:
            return ('{:.' + str(sig_figs - 1) + 'f}').format(0)
        else:
            return 0 
    else:
        DecimalPlaces = -int(math.floor(math.log10(abs(num))) - (sig_figs - 1))
        rounded = round(num, DecimalPlaces)
        if returnstring:
            return ('{:.' + str(DecimalPlaces) + 'f}').format(rounded) # must be string format if you want trailing zero
        else:
            return rounded # return a float instead

def isclosetoint(value, atol=0.00001):
    return math.isclose(value, round(value), rel_tol=0, abs_tol=atol)

def divider_resistance(vfrac, shunt): 
    """Calculates the resistance of an element in a potential divider"""
    return shunt*vfrac/(1 - vfrac)

def is_time_between(begin_time, end_time, check_time=datetime.datetime.now(datetime.timezone.utc).time()): # should work with daylight saving
    """Test if a time is within a given window recuring every day. Take datetime.time objects. Default time to check is current."""
    #print("Check time", check_time, "begin_time", begin_time, "end_time", end_time)
    if begin_time < end_time:
        return check_time >= begin_time and check_time <= end_time
    else: # crosses midnight
        return check_time >= begin_time or check_time <= end_time

class SHthermistor: 
    """Steinhart-Hart thermistor model. Call with resistance to return temperature."""
    # see https://www.thinksrs.com/downloads/programs/Therm%20Calc/NTCCalibrator/NTCcalculator.htm
    # ohms to kelvin, or centigrade if toggled on.

    def __init__(self, constant_offset, constant_ln, constant_ln3, centigrade=True):
        self.c1, self.c2, self.c3, self.centrigrade = constant_offset, constant_ln, constant_ln3, centigrade
    
    def __enter__(self):                                                    # Context manager support eg with block
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __call__(self, resistance):
        temp = 1/(self.c1 + self.c2*ln(resistance) + self.c3*(ln(resistance)**3))
        if self.centrigrade:
            temp -= 273.15
        return temp