# Raspi Zero datalogging of contacts

# Array of electrodes underwater detect level

# Standard imports
import time
from time import sleep

# Installed imports
import RPi.GPIO as io

# Local imports
#none

# Settings
pins = [26, 19, 13, 6, 5, 0, 11, 1, 7, 8, 25] # list of electodes from bottom to top

# Setup 
io.setmode(io.BCM)
io.setup(pins, io.IN, pull_up_down=io.PUD_UP)


# Functions

def readpin(pinnum):
    # Return string 1 or 0 according to state
    if io.input(pinnum):
        return "1"
    else:
        return "0"

def readpins():
    # Reads all pins and concats string
    statestring = ""
    for pinnum in pins:
        statestring += readpin(pinnum)
    return statestring

def loopprintstate():
    try: 
        while True:
            print(readpins())
            sleep(1)
    except KeyboardInterrupt:
        print()
        print()

loopprintstate()
