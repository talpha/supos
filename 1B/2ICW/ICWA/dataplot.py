import matplotlib as mpl
from matplotlib import pyplot as plt

dampings = [
100,
10,
1,
0.7,
0.675,
0.65,
0.6,
0.55,
0.525,
0.51,
0.5,
0.475,
0.4,
0.1,
0.01
]

peaks = [
6.083,
5.136,
2.044,
1.657,
1.628,
1.602,
1.558,
1.528,
1.519,
1.516,
1.515,
1.516,
1.551,
2.959,
5.865
]

times = [
12.42,
10.49,
3.762,
2.97,
2.856,
2.865,
3.015,
3.159,
3.177,
3.186,
3.189,
3.327,
3.801,
10.17,
20.78
]

fig, ax1 = plt.subplots()
#fig = plt.scatter(dampings, peaks)
#ax1 = fig.ax
color = 'tab:red'
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_xlabel("Damping of absorber, Ns/m")
ax1.set_ylabel('Peak response of building, mm', color=color)
ax1.scatter(dampings, peaks,   color=color)
ax1.plot(dampings, peaks,   color=color)
ax1.tick_params(axis='y', labelcolor=color)
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
ax2.set_yscale('log')
color = 'tab:blue'
ax2.set_ylabel('Time to decay to 2%', color=color)
ax2.scatter(dampings, times, color=color)
ax2.plot(dampings, times, color=color)
ax2.tick_params(axis='y', labelcolor=color)
fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.show()