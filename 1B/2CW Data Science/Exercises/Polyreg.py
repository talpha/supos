
# Single function written in Ex1. Hard to import from notebooks so copied into isolation. 

import numpy as np

def polyreg(data_matrix, k):
    
    # It was briefly tempting to go to the numpy source and copy the contents of polyfit
    # Instead, using only inspiration from the first given notebook

    # Validate inputs
    N = len(data_matrix) # This may fail and raise appropriate exception
    if N < 1:
        raise ValueError("Dataset must contain at least one datapoint")
    if data_matrix.ndim != 2:
        raise ValueError("Dataset must be pairs of values - the first the reference and the second the data")
    if type(k) is not int:
        raise TypeError("Degree k must be int, recieved", type(k))
    if k < 0:
        raise ValueError("Degree k must be zero or positive")

    # Handle large k - the unit test block below does not present a test for this requirement.
    if k >= N:
        order = N - 1
    else:
        order = k

    # Extract data
    references = data_matrix[:,0] # First column of array (indexed by 0) 
    datavalues = data_matrix[:,1] # Second column of array (indexed by 1) 

    # Create matrix X which contains all ones in first column, and instreasing powers of data references in the cols thereafter
    X  = np.column_stack(([references**i for i in range(order+1)])) # * unpacks the generator object. The highest power is order. The zeroth order is 'all ones'

    # Computing the coefficient vector beta* using least squares formula
    beta = np.linalg.lstsq(X, datavalues, rcond=None)[0] # copied from given notebook 1, "Fitting a quadratic model  to the CO2 data"
    # This built in function ^ is doing all the work. All I've done here is make a lengthly wrapper - 
    # perhaps this exercise has not gone deep enough to merit not using np.polyfit? Sure we could say beta_lin = np.linalg.inv(XT.dot(X)).dot(XT.dot(yvalues)) but is that really any       better?

    # Compute the fit / reconstruct the data using this model
    fit = X.dot(beta)

    # Calculate residuals
    residuals = datavalues - fit

    # Pad beta with zeros if large k was suppressed
    if k >= N:
        beta = np.append(beta, np.zeros(k - order))
    
    # The function should return the the coefficient vector beta, the fit, and the vector of residuals
    return beta, fit, residuals