"""

Coin flipping in groups
Toby Harris 2020

"""

from random import randint
import matplotlib.pyplot as plt
import numpy as np

flips_per_bunch = 100
bunches = 100000

def coin_flip():
    return randint(0,1)

def bunch(flips): #regardless of how fair the coin is, the bunch result is a continous random variable which is normally distributed according to central limit theorem
    numheads = 0
    for i in range(flips):
        if coin_flip():
            numheads += 1
    return numheads

def manybunches(numbunches, numflips_per_bunch):
    results_list = [0]*(numflips_per_bunch+1)
    for i in range(numbunches):
        results_list[bunch(numflips_per_bunch)] += 1
    return(results_list)

def barchart(results_list):

    plt.bar(range(len(results_list)), results_list)
    plt.xlabel('Heads per bunch')
    plt.ylabel('Occurances / ' + str(bunches))
    plt.title('Distribution of number of heads per group of ' + str(flips_per_bunch))

    plt.show()

barchart(manybunches(bunches, flips_per_bunch))