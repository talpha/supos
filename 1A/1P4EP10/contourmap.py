"""

Contour map of z = f(x,y). 

"""

import numpy as np
import matplotlib.pyplot as plt

#the suggested way:
x = np.arange(-1,3,0.01)
y = np.arange(-1,3,0.01)
X, Y = np.meshgrid(x, y)#, sparse=True)
Z = X*Y*(2-X-2*Y)
plt.contour(X, Y, Z, 200) #contourf gives clear view of polarity without information on gradient. 
plt.show()

