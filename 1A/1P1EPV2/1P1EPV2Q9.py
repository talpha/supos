"""

FEA: Towing a caravan on a bumpy road. Harmonic response case C. 
Plot path of both axle and mass for given road profile.
Toby Harris 2020

Method: 
At each timestep, resolve net force on mass and calculate acceleration. 
Assert acceleration to find displacement at next timestep.

Model:
Assume equlibrium start from flat then begin sinusoidal input.
Consider equlibrium compression of spring - makes flight calcs simpler.
Assume finite but neglible mass of wheel/axle. When off ground, spring force equals dashpot resistance.
Allow infinite suspension travel / k and lambda unaffected by displacement.
Upwards always positive
Use lists a lot as I'm less familiar with np arays

z is integrated backwards but y forwards...

"""

import numpy as np
import matplotlib.pyplot as plt

#setup

    #constants

g = 9.81 #gravitational acceleration, N/kg
m = 500 #mass, kg
l = 2000*np.sqrt(5) #dashpot lambda, Ns/m
k = 20000 #spring constant, N/m
T = 7.5 #input period, m
V = 50 #input velocity, km/h 
X = 0.025 #amplitude of displacement input, m
dt = 0.001 #timestep size
tmax = 1000 #end time        #beware, memory overflows may kick in past 3M data points. Cause unknown.

    #derived constants

omega = 2*np.pi*V/(3.6*T)
natural_omega = np.sqrt(k/m) #not used in calculations - interest only
zeta = l/(2*np.sqrt(k*m)) #not used in calculations - interest only

    #variables

t = [0.0] #empty time list
a = 0 #latest upward acceleration
y = [-m*g/k, -m*g/k] #absolute mass height, list wrt timesteps. Initial condition is steady eqm. 
z = [0, 0] #absolute axle height - same as x (road height) when in contact with ground


#run simulation

    #generate time
while t[-1] < tmax:
    t.append(len(t)*dt) #avoids coumpounding float precisions errors of using t[-1]+dT

    #generate input - sine
x = [X*np.sin(omega*time) for time in t] 

    #deduce response
for disp in x:

        #calculate y position based on previously calculated acceleration
    y.append(y[-1] + (y[-1]-y[-2]) + a*(dt**2)) #s = ut + 1/2 a t**2 ... but really adapted from ds = v dt = (u + at)dt...

        #check ground contact - assume the wheel could move through the ground
    z.append(z[-1] + y[-1] - y[-2] - dt*k*(z[-1] - y[-2])/l)
    
        #force wheel to be on ground if it would be below
    if z[-1] < disp:
        z[-1] = disp #overwrite

        f = k*(z[-1]-y[-1]) + l*(z[-1] - z[-2] - y[-1] + y[-2]) #spring/dash upward force under new conditions
        a = f/m -g
    else: #we're flying
        a = -g


#postprocess

    #remove leading two values (initial conditions) from y and z - the third corresponds to the first timestep.
del y[0:2]
del z[0:2]


#plot

plt.plot(t, y, color='red')
plt.plot(t, z, color='blue')
plt.plot(t, x, color='black')
plt.title(str(("omega ", round(omega, 3), " nat omega ", round(natural_omega, 3), " zeta ", round(zeta, 3), " dt ", dt))) #not pretty, but will do
plt.show()