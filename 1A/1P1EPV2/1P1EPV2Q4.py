"""

resonance fit from data points

"""

import numpy as np
import matplotlib.pyplot as plt

resonantfrequency = 20 #Hz
frequencieshz = [0, 14, 16, 18, 20, 22, 24, 26]
amplititudes = [0.0001, 0.00019, 0.00024, 0.00034, 0.0004, 0.00029, 0.00019, 0.00013]

frequenciesrad = [i*2*np.pi for i in frequencieshz]
magnitudes = [i/amplititudes[0] for i in amplititudes] #could look up corresponding to frequency = 0 ...

zeta = 0.126 #calculated manually
naturalfrequency = 20*2*np.pi/np.sqrt(1-2*zeta**2)

frequenciesrel = [i/naturalfrequency for i in frequenciesrad]

x = np.linspace(0, 2, 1000)
a = 1 - x**2
b = 2*zeta*x

y = 1/np.sqrt((a**2) + (b**2))

plt.plot(frequenciesrel, magnitudes, 'o', color='black') #miss my u
plt.plot(x, y)
plt.axvline(x=1, color='black')
plt.show()